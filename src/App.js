import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
// import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core Components

// import styles from "../src/assets/dashboardStyle.js";
import ReportDisplay from "./Components/reportDisplay";
// import TableTest from "./Components/TableTest";
import testing from "./Components/reportDisplay";
import componentDidMount from "./Components/reportDisplay";
// import styles from "/src/assets/jss/material-dashboard-react/views/dashboardStyle.js";

// const classes = makeStyles(styles);

export default class App extends React.Component {
  componentDidMount() {
    axios.get("http://localhost:4000/report").then(res => {
      // declerations
      const reports = res.data;
      this.setState({ reports });

      const openVulnerabilities = res.data[1];
      const closedVulnerabilities = res.data[0];
      let globals = window["globals"];
      globals.openVulnerabilities = openVulnerabilities;
      globals.closedVulnerabilities = closedVulnerabilities;

      const totals =
        closedVulnerabilities.adminServices +
        closedVulnerabilities.collaborationAndOfficeServices +
        closedVulnerabilities.financeAndProcurementServices +
        closedVulnerabilities.peopleAndRAS +
        closedVulnerabilities.monitoringAndPerformanceSystemsServices +
        openVulnerabilities.adminServices +
        openVulnerabilities.collaborationAndOfficeServices +
        openVulnerabilities.financeAndProcurementServices +
        openVulnerabilities.peopleAndRAS +
        openVulnerabilities.monitoringAndPerformanceSystemsServices;
      globals.totals = totals;
      this.setState({ reports, totals });
      // ReactDOM.render(totals, document.getElementById("totals"));

      const closed =
        res.data[0].adminServices +
        res.data[0].collaborationAndOfficeServices +
        res.data[0].financeAndProcurementServices +
        res.data[0].peopleAndRAS +
        res.data[0].monitoringAndPerformanceSystemsServices;
      // ReactDOM.render(closed, document.getElementById("cv"));

      // ReactDOM.render(
      //   parseFloat((closed / totals) * 100).toFixed(2) + "%",
      //   document.getElementById("prec")
      // );
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      reports: [{}]
    };
  }

  render() {
    // let globals = window["globals"];
    // const classes = useStyles();

    return (
      <div className="App">
        <div>
          <h1>{this.state.totals}</h1>
        </div>
        <br />
        <div id="totals"></div>
        <br />
        <div id="cv"></div>
        <br />
        <br /> Table 1 data
        <br /> Table 2 data
        {/* <TableTest data={this.state.reports} /> */}
      </div>
    );
  }
}
